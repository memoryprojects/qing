﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	//地面の変数
	private GameObject _ground;
	private float _groundPosX;
	private float _groundScaleX;
	private float _groundPosZ;
	private float _groundScaleZ;

	//プロパティ
	public GameObject Ground{ get{ return _ground;}}
	public float GroundPosX{ get{ return _groundPosX;}}
	public float GroundScaleX{ get{ return _groundScaleX;}}
	public float GroundPosZ{ get{ return _groundPosZ;}}
	public float GroundScaleZ{ get{ return _groundScaleZ;}}

	// Use this for initialization
	public GameManager () {
		//地面オブジェクトとスケールを取得
		_ground = GameObject.FindWithTag("Ground");
		_groundPosX = _ground.transform.position.x;
		_groundScaleX = _ground.transform.localScale.x;
		_groundPosZ = _ground.transform.position.z;
		_groundScaleZ = _ground.transform.localScale.z;
	}

	public static GameManager _singleton;
	public static GameManager GetInstance(){
		return _singleton ?? (_singleton = new GameManager ());
	}

	public GameObject GetUiObject(string name){
		return GameObject.Find ("Canvas/Panel/" + name);
	}


}
