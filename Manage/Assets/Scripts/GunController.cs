﻿using UnityEngine;
using System.Collections;

public class GunController : MonoBehaviour {
	public const float ROT_ANG = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.I)) {
			transform.Rotate (-ROT_ANG, 0, 0);
		}
		if (Input.GetKey (KeyCode.K)) {
			transform.Rotate (ROT_ANG, 0, 0);
		}
	}
}
