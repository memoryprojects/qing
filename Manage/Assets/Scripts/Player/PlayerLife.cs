﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerLife : MonoBehaviour {
	[HideInInspector]public const int MAX_HP = 200;
	[HideInInspector]public const float MOMENT_TIME = 0.2f;
	private int _currentLife;
	private Text _lifeText;
	private bool _dead;

	// Use this for initialization
	void Start () {
		_currentLife = MAX_HP;
		_lifeText = GameManager.GetInstance ().GetUiObject (this.gameObject.name + "/Life").GetComponent<Text>();
		_lifeText.text = "Life:" + _currentLife + "";
		_dead = false;
	}
	
	// Update is called once per frame
	void Update () {
	}

	void Hit(){
		if (_currentLife > 0) {
			_currentLife--;
			_lifeText.text = "Life:" + _currentLife + "";
		} else {
			StartCoroutine ("Dead");
		}
	}

	private IEnumerator Dead(){
		_lifeText.text = "DEAD";
		GetComponent<Player> ().Dead ();
		GetComponent<PlayerShot> ().Dead ();
		while (true) {
			yield return new WaitForSeconds (MOMENT_TIME);
			transform.localScale *= 0.9f;
			if (transform.localScale.magnitude <= 0.2f) {
				enabled = true;
			}
		}
	}
}
