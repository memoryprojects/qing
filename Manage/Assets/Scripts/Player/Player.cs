﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	[HideInInspector]public const float MINUS_SPEED = 0.8f;  //移動、回転の減速に利用
	//回転に関する変数
	[HideInInspector]public const float ADD_ANGLE = 0.2f; 
	[HideInInspector]public const float ANGLE_MAX = 4f;
	private float _rotateAngle;
	private bool _rightRot;
	private bool _leftRot;
	private bool _dead;

	//移動に関する変数
	[HideInInspector]public const float ADD_SPEED = 0.1f;
	[HideInInspector]public const float MAX_SPEED = 1f;
	private float _moveSpeed;
	private bool _forward;
	private bool _back;


	// Use this for initialization
	void Start () {
		//回転の変数の初期化
		_rotateAngle = 0;
		_rightRot = false;
		_leftRot = false;

		//移動の変数の初期化
		_moveSpeed = 0;
		_forward = false;
		_back = false;
		_dead = false;
	}
	
	// Update is called once per frame
	void Update () {
		//左回転のフラグ管理
		if (Input.GetKeyDown (KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) {
			_leftRot = true;
		}
		if (Input.GetKeyUp (KeyCode.A) || Input.GetKeyUp (KeyCode.LeftArrow)) {
			_leftRot = false;
		}

		//右回転のフラグ管理
		if (Input.GetKeyDown (KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) {
			_rightRot = true;
		}
		if (Input.GetKeyUp (KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow)) {
			_rightRot = false;
		}

		//フラグに基づく回転速度制御
		if (_leftRot) {
			if (-ANGLE_MAX <= _rotateAngle)
				_rotateAngle -= ADD_ANGLE;
		} else if (_rightRot) {
			if (ANGLE_MAX >= _rotateAngle)
				_rotateAngle += ADD_ANGLE;
		} else {
			_rotateAngle *= MINUS_SPEED;
		}

		//計算結果を座標に反映
		if(_dead == false)
			transform.Rotate(0, _rotateAngle, 0);

		//前方移動のフラグ管理
		if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)){
			_forward = true;
		}
		if (Input.GetKeyUp (KeyCode.W) || Input.GetKeyUp (KeyCode.UpArrow)) {
			_forward = false;
		}

		//後方移動のフラグ管理
		if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)){
			_back = true;
		}
		if (Input.GetKeyUp (KeyCode.S) || Input.GetKeyUp (KeyCode.DownArrow)) {
			_back = false;
		}

		//フラグに基づく移動速度制御
		if (_forward) {
			if (MAX_SPEED >= _moveSpeed)
				_moveSpeed += ADD_SPEED;
		} else if (_back) {
			if (-MAX_SPEED <= _moveSpeed)
				_moveSpeed -= ADD_SPEED;
		} else {
			_moveSpeed *= MINUS_SPEED;
		}

		//計算結果を座標に反映
		if(_dead == false)
			transform.Translate(0, 0, _moveSpeed);

		//はみ出ないように制御
		float minX = GameManager.GetInstance().GroundPosX - GameManager.GetInstance().GroundScaleX / 2;
		float maxX = GameManager.GetInstance().GroundPosX + GameManager.GetInstance().GroundScaleX / 2;
		float minZ = GameManager.GetInstance().GroundPosZ - GameManager.GetInstance().GroundScaleZ / 2;
		float maxZ = GameManager.GetInstance().GroundPosZ + GameManager.GetInstance().GroundScaleZ / 2;
		transform.position = new Vector3 (Mathf.Clamp (transform.position.x, minX, maxX), transform.position.y, 
			Mathf.Clamp (transform.position.z, minZ, maxZ));
	}

	public void Dead(){
		_dead = true;
	}
}
