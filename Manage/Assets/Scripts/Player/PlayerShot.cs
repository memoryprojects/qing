﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerShot : MonoBehaviour {
	public const int FIRST_COIN = 5;
	public const int MAX_COIN = 20;
	public const float FORCE = 120;
	private int _currentCoin;
	private GameObject _prefabCoin;
	private GameObject _shotObj;
	private Text _remainingText;
	private bool dead;

	// Use this for initialization
	void Start () {
		_currentCoin = FIRST_COIN;
		_prefabCoin = Resources.Load ("Prefabs/Shot") as GameObject;
		_shotObj = transform.FindChild ("Shot").gameObject;
		_remainingText = GameManager.GetInstance ().GetUiObject (this.gameObject.name + "/ShotText").GetComponent<Text> ();
		_remainingText.text = "残り " + _currentCoin + "" + "枚";
		dead = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space) && dead == false) {
			Shot ();
		}
	}

	public void Dead(){
		dead = true;
	}

	public void GetCoin(){
		if (_currentCoin < MAX_COIN) {
			_currentCoin++;
			_remainingText.text = "残り " + _currentCoin + "" + "枚";
			if (_currentCoin == MAX_COIN) {
				_remainingText.color = Color.red;
			} else {
				_remainingText.color = Color.black;
			}
		}
	}

	public void Shot(){
		if (_currentCoin > 0) {
			_currentCoin--;
			GameObject c = (GameObject)Instantiate (_prefabCoin, _shotObj.transform.position, Quaternion.identity);
			Vector3 v = _shotObj.transform.forward;
			c.GetComponent<Rigidbody> ().AddForce (v * FORCE, ForceMode.Impulse);
			_remainingText.text = "残り " + _currentCoin + "" + "枚";
			if (_currentCoin == 0) {
				_remainingText.color = Color.red;
			} else {
				_remainingText.color = Color.black;
			}
		}
	}
}
