﻿using UnityEngine;
using System.Collections;

public class CoinManager : MonoBehaviour {
	GameObject[] _coins;
	[HideInInspector] public const int MAX_COIN = 10;
	private GameObject _prefabCoin;

	// Use this for initialization
	void Start () {
		_prefabCoin = Resources.Load ("Prefabs/Coin") as GameObject;
		_coins = new GameObject[MAX_COIN];
		for (int i = 0; i < MAX_COIN; i++) {
			_coins [i] = GameObject.Instantiate (_prefabCoin, new Vector3 (0, 100, 0), Quaternion.identity) as GameObject;
			_coins [i].GetComponent<Coin> ().AssignMyNumber (i);
		}
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void ReSporn (int number){
		Destroy (_coins [number].gameObject);
		_coins[number] = GameObject.Instantiate (_prefabCoin, new Vector3 (0, 100, 0), Quaternion.identity) as GameObject;
		_coins [number].GetComponent<Coin> ().AssignMyNumber (number);
	}
}
