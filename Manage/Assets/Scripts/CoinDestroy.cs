﻿using UnityEngine;
using System.Collections;

public class CoinDestroy : MonoBehaviour {
	public const float RIMIT_TIME = 5f;
	void Start(){
		StartCoroutine ("Destroy");
	}

	private IEnumerator Destroy(){
		yield return new WaitForSeconds (RIMIT_TIME);
		Destroy (this.gameObject);
	}

	void OnCollisionEnter(Collision other){
		if (other.gameObject.tag == "Ground") {
			Destroy (this.gameObject);
		}
	}
}
