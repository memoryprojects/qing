﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {
	//前提準備
	[SerializeField]private GameObject[] _players;
	private int _state;
	[HideInInspector]public const int SEARCH_STATE = 0;
	[HideInInspector]public const int MOVE_STATE = 1;
	private GameObject _effectParticle;
	private GameObject _ground;
	private int _myNumber;

	//探索状態
	private GameObject _targetPlayer;
	[Range(0, 100)]public float _range;

	//移動状態
	[HideInInspector]public const float EASING = 1f;
	[Range(0, 1f)]public float _speed;
	private float _timeSpeed;


	// Use this for initialization
	void Start () {
		_state = SEARCH_STATE;
		_players = GameObject.FindGameObjectsWithTag ("Player");
		_effectParticle = transform.FindChild ("Effect").gameObject;
		_effectParticle.SetActive (false);
		StartCoroutine("NewPosition");
		_timeSpeed = 0.5f;
	}

	public void AssignMyNumber(int num){
		_myNumber = num;
	}

	private IEnumerator NewPosition(){
		yield return new WaitForSeconds (0.001f);
		float xPos = GameManager.GetInstance ().GroundPosX;
		float xRange = GameManager.GetInstance ().GroundScaleX;
		float zPos = GameManager.GetInstance ().GroundPosZ;
		float zRange = GameManager.GetInstance ().GroundScaleZ;
		transform.position = new Vector3 (Random.Range (xPos - xRange / 2, xPos + xRange / 2), 10, 
			Random.Range(zPos - zRange / 2, zPos + zRange / 2));
		_state = SEARCH_STATE;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 myPos = transform.position;  //自分の位置を格納

		switch (_state) {
		case SEARCH_STATE:
			//変数の用意
			Vector3 playerPos;
			float dx, dy, dz, dist;
			//プレイヤーとコインで3点館の距離を測る
			for (int i = 0; i < _players.Length; i++) {
				playerPos = _players [i].transform.position;
				dx = Mathf.Pow(playerPos.x - myPos.x, 2);
				dy = Mathf.Pow(playerPos.y - myPos.y, 2);
				dz = Mathf.Pow(playerPos.z - myPos.z, 2);
				dist = Mathf.Sqrt (dx + dy + dz);
				//一定の距離以下だった場合、そのプレイヤーを標的にする
				if (dist <= _range) {
					_targetPlayer = _players [i];
					_state = MOVE_STATE;
					_effectParticle.SetActive (true);
				}
			}
			break;
		case MOVE_STATE:
			//イージングをしてプレイヤーに近く
			_timeSpeed += Time.deltaTime * 2;
			Vector3 targetPos = _targetPlayer.transform.position;
			Vector3 v = (targetPos - myPos) * EASING * _speed * _timeSpeed;
			transform.position += v;
			break;
		}
	}

	void OnCollisionEnter(Collision other){
		if (other.gameObject.tag == "Player") {
			other.gameObject.GetComponent<PlayerShot> ().GetCoin ();
			GameObject.Find ("CoinManager").GetComponent<CoinManager> ().ReSporn (_myNumber);
		}
	}
}
